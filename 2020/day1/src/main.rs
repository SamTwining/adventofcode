use std::io::Error;
use std::fs::File;
use rust_utils::arrayutils::filetoarray;

fn get_vector_from_file(filename: String) -> Vec<i64> {
  let file = File::open(filename).unwrap();
  let consumed_arry: Result<Vec<i64>, Error> = filetoarray::<File>(file);
  let array: Vec<i64> = consumed_arry.unwrap();

  return array;
}

fn main() {
  let filename : String = "C:\\Users\\Sam\\repos\\aoc-2020\\day1\\src\\input.txt".to_string();
  let file_vec : Vec<i64> = get_vector_from_file(filename);
  let mut result: Option<(i64, i64)> = None;
  
  for f in &file_vec {
    let opt = file_vec.iter().find(|&x| x + f == 2020);
    match opt {
      Some(p) => { result = Some((*p, *f)); break; },
      None => {}
    }
  }
  
  match result {
    Some(p) => println!("Found values {} and {}, {} x {} = {}", p.0, p.1, p.0, p.1, p.0 * p.1),
    None => println!("No matches found :(")
  }
}