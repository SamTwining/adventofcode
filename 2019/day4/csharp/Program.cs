﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace day4
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] logLines = File.ReadAllLines("./input");
            var eventList = logLines.Select(x => new Event(x));

            Dictionary<int, List<int>> GuardDict = new Dictionary<int, List<int>>();
            int currGuardId = 0;
            DateTime timeFellAsleep = DateTime.Now;


            foreach(Event logLine in eventList.OrderBy(x => x.EventTime)) 
            {
                switch(logLine.Type) 
                {
                    case EventType.ShiftChange:
                        currGuardId = getCurrentGuardId(logLine, GuardDict);
                        break;
                    case EventType.Sleep:
                        timeFellAsleep = logLine.EventTime;
                        break;
                    case EventType.Awaken:
                        setSleepLog(GuardDict, currGuardId, timeFellAsleep, logLine.EventTime);
                        break;
                }
            }

            var mostSleepy = GuardDict.Aggregate((l, r) => l.Value.Count > r.Value.Count ? l : r);
            int mostSleptMinute = getMostFrequentElementGroup(mostSleepy.Value).Key;

            Console.WriteLine($"Most sleepy guard is {mostSleepy.Key} with {mostSleepy.Value.Count} slept minutes.");
            Console.WriteLine($"He slept most during minute {mostSleptMinute}");
            Console.WriteLine($"Answer: {mostSleptMinute * mostSleepy.Key}");

            int mostHabitualSleeperId = 0;
            int mostSleptMinuteFrequencyTop = 0;
            foreach(var guardKV in GuardDict) 
            {
                if(guardKV.Value.Count == 0) continue;
                IGrouping<int, int> mostSleptMinuteGroup = getMostFrequentElementGroup(guardKV.Value);
                int individualMostSleptMinute = mostSleptMinuteGroup.Key;
                int mostSleptMinuteFrequency = mostSleptMinuteGroup.Count();
                if(mostSleptMinuteFrequency > mostSleptMinuteFrequencyTop)
                {
                    mostSleptMinuteFrequencyTop = mostSleptMinuteFrequency;
                    mostSleptMinute = individualMostSleptMinute;
                    mostHabitualSleeperId = guardKV.Key;
                }

            }

            Console.WriteLine($"Most habitual sleeper was {mostHabitualSleeperId} with {mostSleptMinute} {mostSleptMinuteFrequencyTop} times");
            Console.WriteLine($"Answer: {mostHabitualSleeperId * mostSleptMinute}");
        }

        static IGrouping<int, int> getMostFrequentElementGroup(List<int> arr) 
        {
            // if(arr.Count == 0) return null;
            return arr.GroupBy(x => x).OrderByDescending(x => x.Count()).FirstOrDefault();
        }

        static void setSleepLog(Dictionary<int, List<int>> guardDict, int guardId, DateTime startSleep, DateTime endSleep) 
        {
            IEnumerable<int> sleptMinutes = Enumerable.Range(startSleep.Minute, endSleep.Minute - startSleep.Minute);
            guardDict[guardId].AddRange(sleptMinutes);
        }

        static int getCurrentGuardId(Event shiftEvent, Dictionary<int, List<int>> guardDict) 
        {
            Match idMatch = Regex.Match(shiftEvent.Log, @"[\d]+");
            int guardId = 0;
            try
            {
                guardId = int.Parse(idMatch.Value);
                if(!guardDict.ContainsKey(guardId))
                {
                    guardDict[guardId] = new List<int>();
                }
            }
            catch(Exception e) 
            {
                Console.WriteLine($"Error parsing Guard ID  {idMatch.Value}");
                Console.WriteLine(e);
            }

            return guardId;
        }
    }

    public enum EventType { Invalid, ShiftChange, Awaken, Sleep }
    public class Event
    {
        public Event(string eventString) 
        {
            this.EventTime = DateTime.ParseExact(eventString.Substring(1, 16), "yyyy-MM-dd HH:mm", CultureInfo.CurrentCulture);
            this.Log = eventString.Substring(19);

            // Better or worse than a bunch of if statements?
            // Think with this formatting it's fairly readable.
            // Then again...
            this.Type = IsNewShift ? EventType.ShiftChange : 
                            IsAsleepEvent ? EventType.Sleep : 
                            IsAwakenEvent ? EventType.Awaken : 
                            EventType.Invalid;
        }

        public EventType Type { get; set; }

        public bool IsNewShift
        {
            get { return this.Log.Contains("begins shift"); }
        }

        public bool IsAsleepEvent
        {
            get { return this.Log.Contains("falls asleep"); }
        }

        public bool IsAwakenEvent 
        {
            get { return this.Log.Contains("wakes up"); }
        }

        public DateTime EventTime {get;set;}
        public string Log {get;set;}
    }

}
