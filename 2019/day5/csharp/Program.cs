﻿using System;
using System.IO;
using System.Linq;
namespace day5
{
    class Program
    {
        static void Main(string[] args)
        {            
            string polymer = File.ReadAllText("./input");

            bool keepRunning = true;
            while(keepRunning) 
            {
                bool foundMatch = false;
                for(int i = 0; i < polymer.Length; i++) 
                {
                    if(i == 0)
                        continue;
                    char t = polymer[i];
                    
                    if(Math.Abs((int)polymer[i] - (int)polymer[i-1]) == 32) 
                    {
                        polymer = polymer.Remove(i-1, 2);
                        foundMatch = true;
                        break;
                    }
                }
                
                keepRunning = foundMatch;
            }

            Console.WriteLine($"Remainder: {polymer.Length}");

            polymer = File.ReadAllText("./input");
            int shortest = Int32.MaxValue;
            for(int i = (int)'a'; i <= (int)'z'; i++) 
            {

                string newPolymer = new string(polymer.Where(x => !(new char[] { (char)i, (char)(i-32) }.Contains(x))).ToArray());

                bool keepRunning = true;
                while(keepRunning) 
                {
                    bool foundMatch = false;
                    for(int j = 0; j < newPolymer.Length; j++) 
                    {
                        if(j == 0)
                            continue;
                        
                        if(Math.Abs((int)newPolymer[j] - (int)newPolymer[j-1]) == 32) 
                        {
                            newPolymer = newPolymer.Remove(j-1, 2);
                            foundMatch = true;
                            break;
                        }
                    }
                    
                    keepRunning = foundMatch;
                }

                if(newPolymer.Length < shortest) {
                    Console.WriteLine($"New shortest polymer is length {newPolymer.Length} for {(char)i} and {(char)(i-32)}");
                    shortest = newPolymer.Length;
                }

            }

            Console.WriteLine($"Shortest polymer is {shortest}");
        }

    }
}
