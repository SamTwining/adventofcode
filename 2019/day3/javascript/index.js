var f=(require('fs').readFileSync('./input')+"").split('\n');

var arr = [];
for(var i = 0; i < 1000; i++) {
    arr[i] = new Array(1000).fill(0);
}

function getClaimObj(claim) {
    var indexOfX = claim.indexOf("x"),
    indexOfAt = claim.indexOf("@"),
    location = claim.slice(indexOfAt+2, claim.indexOf(":")).split(","),
    dimensions = claim.slice(indexOfX-2, indexOfX+3).split("x"),
    count = dimensions[0]*dimensions[1];
    return {
        location: location,
        dimensions: dimensions,
        count: count
    }
}

function getClaimAbsoluteLocation(claim, index) {
    return {
        x: (index % claim.dimensions[0]) + claim.location[0]*1,
        y: ((index/claim.dimensions[0]) |0)+(claim.location[1]*1)
    }
}

function operateOnClaim(func, checkValidity) {
    for(var i = 0; i < f.length; i++) {
        if(!f[i]) continue;

        var claim = getClaimObj(f[i]),
        validClaim = true;

        for(var j = 0; j < claim.count; j++) {
            var absoluteLocation = getClaimAbsoluteLocation(claim, j);
            var stillValid = func(absoluteLocation.x, absoluteLocation.y);
            if(checkValidity && !stillValid) {
                validClaim = false;
            }
        }

        if(checkValidity && validClaim)
            console.log(f[i] + " is valid!");
    }
}

operateOnClaim(function(x, y) { 
    arr[x][y]++;
});

operateOnClaim(function(x, y) {
    return arr[x][y] == 1;
}, true);

console.log(arr.flat().filter(function(elem) { return elem > 1; }).length + " double claimed sections");